/// Day 1

use std::io::BufReader;
use std::io::BufRead;
use std::fs::File;
use std::collections::HashSet;

fn load_ops(file_path: &str) -> Result<Vec<i32>, Box<std::error::Error + 'static>> {
    let mut ops : Vec<i32> = Vec::new();
    let f = File::open(file_path)?;

    let file = BufReader::new(&f);
    for line in file.lines() {
        let l = line?;
        ops.push(l.parse::<i32>()?);
    }

    Ok(ops)
}

pub fn solve(freq_file: &str) -> Result<(), Box<std::error::Error + 'static>> {
    let ops = load_ops(freq_file)?;

    let mut freq : i32 = 0;
    let mut seen = HashSet::new();

    let mut found_repeated_freq = false;
    let mut done_first_pass = false;

    while !found_repeated_freq {
        for op in &ops {
            freq += op;

            if seen.contains(&freq) {
                println!("Puzzle 2 frequency: {}", freq);
                found_repeated_freq = true;
                break;
            } else {
                seen.insert(freq);
            }
        }

        if ! done_first_pass {
            println!("Puzzle 1 frequency: {}", freq);
            done_first_pass = true;
        }
    }

    Ok(())
}
