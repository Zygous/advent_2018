// Day 4
use regex::Regex;
use std::cmp::Ordering;
use std::collections::HashMap;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

#[derive(Debug, Eq)]
enum EventType {
    StartedShift,
    StartedSleeping,
    Awoke,
}
impl PartialEq for EventType {
    fn eq(&self, other: &EventType) -> bool {
        match self {
            EventType::StartedShift => match &other {
                EventType::StartedShift => true,
                _ => false,
            },
            EventType::StartedSleeping => match &other {
                EventType::StartedSleeping => true,
                _ => false,
            },
            EventType::Awoke => match &other {
                EventType::Awoke => true,
                _ => false,
            },
        }
    }
}

#[derive(Debug, Eq)]
struct Event {
    date: String,
    minute: i32,
    guard_id: Option<i32>,
    event_type: EventType,
}
impl Ord for Event {
    fn cmp(&self, other: &Event) -> Ordering {
        let date_cmp = self.date.cmp(&other.date);
        if date_cmp != Ordering::Equal {
            return date_cmp;
        }

        return self.minute.cmp(&other.minute);
    }
}
impl PartialOrd for Event {
    fn partial_cmp(&self, other: &Event) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl PartialEq for Event {
    fn eq(&self, other: &Event) -> bool {
        self.date == other.date
    }
}

fn load_events(events_file: &str) -> Result<Vec<Event>, Box<std::error::Error>> {
    let mut events: Vec<Event> = Vec::new();

    // Regex crate doesn't support alternates with empty cases, e.g.
    // (?:|Guard ), so this is even uglier than it might have been!
    let re = Regex::new(
        //       1           2       3       4         5 6
        r"\[1518-(\d\d-\d\d) (23|00):(\d\d)\]( Guard)? (#(\d+)|falls|wakes)",
    )
    .unwrap();

    let f = File::open(events_file)?;
    let file = BufReader::new(&f);
    for line in file.lines() {
        let l = line?;

        if let Some(captures) = re.captures(&l) {
            let event_type: Option<EventType>;

            match captures.get(4) {
                Some(_) => event_type = Some(EventType::StartedShift),
                None => {
                    event_type = match captures.get(5).unwrap().as_str() {
                        "falls" => Some(EventType::StartedSleeping),
                        "wakes" => Some(EventType::Awoke),
                        &_ => None,
                    };
                }
            }

            match event_type {
                Some(event_type) => events.push(Event {
                    date: captures.get(1).unwrap().as_str().to_owned(),
                    minute: {
                        match captures.get(2).unwrap().as_str().parse::<i32>()? {
                            23 => captures.get(3).unwrap().as_str().parse::<i32>()? - 60,
                            0 => captures.get(3).unwrap().as_str().parse::<i32>()?,
                            _ => 0
                        }
                    },
                    guard_id: if let Some(i) = captures.get(6) {
                        Some(i.as_str().parse::<i32>()?)
                    } else {
                        None
                    },
                    event_type,
                }),
                None => (),
            }
        }
    }

    events.sort();

    Ok(events)
}

fn max(map: &HashMap<i32, i32>) -> (i32, i32) {
    let mut sorted : Vec<_> = map.iter().collect();

    // Sort by index first, so that in a tie we pick the smallest by index.
    sorted.sort_by(|a, b| a.0.cmp(b.0));
    sorted.sort_by(|a, b| a.1.cmp(b.1).reverse());

    (*sorted[0].0, *sorted[0].1)
}

fn part_2(guard_sleep_min_counts: HashMap<i32, HashMap<i32, i32>>) {
    let mut max_count = 0;
    let mut max_min = 0;
    let mut max_id = 0;

    for (id, min_counts) in guard_sleep_min_counts {
        let (min, count) = max(&min_counts);
        if count > max_count {
            max_count = count;
            max_min = min;
            max_id = id;
        }
    }

    println!("Guard #{} was most consistently asleep at 00:{}", max_id, max_min);
    println!("Answer for part 2: {}", max_id * max_min);
}

pub fn solve(file_path: &str) -> Result<(), Box<std::error::Error>> {
    let events = load_events(file_path)?;

    // Map guard_id -> minute -> count
    let mut guard_sleep_min_counts: HashMap<i32, HashMap<i32, i32>> = HashMap::new();
    // For convenience, map guard_id -> mins slept
    let mut guard_sleep_times: HashMap<i32, i32> = HashMap::new();

    let mut start_min: i32 = 0;
    let mut guard_id: i32 = 0;
    for event in events {
        match event.event_type {
            EventType::StartedShift => match event.guard_id {
                Some(id) => guard_id = id,
                None => guard_id = 0,
            },
            EventType::StartedSleeping => start_min = event.minute,
            EventType::Awoke => {
                let guard_mins = guard_sleep_times.entry(guard_id).or_insert(0);
                let mins_slept = (event.minute - 1) - start_min;
                *guard_mins += mins_slept;

                let min_counts = guard_sleep_min_counts
                    .entry(guard_id)
                    .or_insert(HashMap::new());

                for i in start_min..event.minute {
                    let count = min_counts.entry(i).or_insert(0);
                    *count += 1;
                }
            }
        }
    }

    let sleepiest_guard = max(&guard_sleep_times).0;
    let most_slept_min = max(guard_sleep_min_counts.get(&sleepiest_guard).unwrap()).0;
    println!(
        "The sleepiest guard is #{}. He was most often asleep at 00{}",
        sleepiest_guard, most_slept_min
    );
    println!("Answer for part 1: {}", sleepiest_guard * most_slept_min);

    part_2(guard_sleep_min_counts);

    Ok(())
}
