Advent of Code 2018
==

These are my solutions to the
[2018 Advent of Code](https://adventofcode.com/), built as my first
attempt to do something with [Rust](https://www.rust-lang.org/).

To run the application specify a day number followed by any contextual
arguments required for that day's puzzle (I expect this might change
day-to-day). E.g. to solve the first day's puzzles, from the project root
directory:

```sh
$ cargo run 1 inputs/day_1.txt
```

Alternatively you could build the application and then run it:

```sh
$ cargo build --release
$ ./target/release/advent_2018 1 inputs/day_1.txt
```
