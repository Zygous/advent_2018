/// Day 2

use std::collections::HashMap;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn load_codes(file_path: &str) -> Result<Vec<Vec<char>>, Box<std::error::Error + 'static>> {
    let mut codes: Vec<Vec<char>> = Vec::new();
    let f = File::open(file_path)?;

    let file = BufReader::new(&f);
    for line in file.lines() {
        let l = line?;
        let code_chars: Vec<char> = l.chars().collect();
        codes.push(code_chars);
    }

    Ok(codes)
}

fn code_checksum_value(chars: Vec<char>) -> (i32, i32) {
    let mut d = 0;
    let mut t = 0;
    let mut buckets = HashMap::new();

    for c in chars {
        let seen;
        {
            seen = buckets.contains_key(&c);
        }
        if seen {
            let i;
            {
                i = buckets.get(&c).unwrap() + 1;
            }
            buckets.insert(c, i);
        } else {
            buckets.insert(c, 1);
        }
    }

    for v in buckets.values() {
        if *v == 2 {
            d = 1;
        } else if *v == 3 {
            t = 1;
        }
    }

    (d, t)
}

fn print_checksum(codes: &Vec<Vec<char>>) {
    let mut doubles = 0;
    let mut triples = 0;

    for code in codes {
        let (doubles_inc, triples_inc) = code_checksum_value(code.to_vec());
        doubles += doubles_inc;
        triples += triples_inc;
    }

    println!("Codes checksum: {}", doubles * triples);
}

fn solve_part_2(codes: &Vec<Vec<char>>) {
    let len = codes[0].len();

    for i in 0..len {
        let mut shortened : Vec<Vec<char>> = Vec::new();
        for code in codes {
            let mut c = code.to_vec();
            c.remove(i);
            shortened.push(c);
        }

        shortened.sort();

        let mut deduped = shortened.clone();
        deduped.dedup();

        if deduped.len() < shortened.len() {
            for window in shortened.windows(2) {
                if window[0] == window[1] {
                    println!("Common crate ID letters: {}", &window[0].iter().collect::<String>());
                    return;
                }
            }
        }
    }
}

pub fn solve(codes_file: &str) -> Result<(), Box<std::error::Error + 'static>> {
    let codes = load_codes(codes_file)?;

    print_checksum(&codes);
    solve_part_2(&codes);

    Ok(())
}
