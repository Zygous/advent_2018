extern crate regex;

use std::env;

mod day_1;
mod day_2;
mod day_3;
mod day_4;

fn main() -> Result<(), Box<std::error::Error + 'static>> {
    let args: Vec<_> = env::args().collect();

    if args.len() > 1 {
        match &*args[1] {
            "1" => day_1::solve(&*args[2])?,
            "2" => day_2::solve(&*args[2])?,
            "3" => day_3::solve(&*args[2])?,
            "4" => day_4::solve(&*args[2])?,
            d => println!("No solutions for day {}", d),
        };
    } else {
        println!("Specify a day number and path to puzzle input file.");
    }

    Ok(())
}
