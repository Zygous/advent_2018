/// Day 3
use regex::Regex;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

#[derive(Debug)]
struct Point { x: usize, y: usize }

#[derive(Debug)]
struct Claim { origin: Point, end: Point }

fn load_claims(claims_file: &str) -> Result<
    (HashMap<usize, Claim>, usize, usize),
    Box<std::error::Error + 'static>
> {
    let mut claims : HashMap<usize, Claim> = HashMap::new();
    let mut total_x : usize = 0;
    let mut total_y : usize = 0;

    // Line format:       #<id> @ <x>,<y>: <w>x<h>
    let re = Regex::new(r"#(\d+) @ (\d+),(\d+): (\d+)x(\d+)").unwrap();

    let f = File::open(claims_file)?;

    let file = BufReader::new(&f);
    for line in file.lines() {
        let l = line?;

        if let Some(captures) = re.captures(&l) {
            let id = captures.get(1).unwrap().as_str().parse::<usize>()?;
            let x = captures.get(2).unwrap().as_str().parse::<usize>()?;
            let y = captures.get(3).unwrap().as_str().parse::<usize>()?;
            let width = captures.get(4).unwrap().as_str().parse::<usize>()?;
            let height = captures.get(5).unwrap().as_str().parse::<usize>()?;
            let xmax = x + width;
            let ymax = y + height;

            total_x = total_x.max(xmax);
            total_y = total_y.max(ymax);

            claims.insert(id, Claim{
                origin: Point{ x, y },
                end: Point { x: xmax, y: ymax }
            });
        };
    }

    Ok((claims, total_x, total_y))
}

pub fn solve(claims_file: &str) -> Result<(), Box<std::error::Error + 'static>> {
    let (claims, total_x, total_y) = load_claims(claims_file)?;

    println!("Grid size: {}x{}", total_x, total_y);

    let mut seen : HashMap<(usize, usize), usize> = HashMap::new();
    let mut overlapped = 0;

    let mut ids : HashSet<usize> = HashSet::new();
    let mut overlapping_ids : HashSet<usize> = HashSet::new();

    for (claim_id, claim) in claims.iter() {
        ids.insert(*claim_id);

        for x in claim.origin.x .. claim.end.x {
            for y in claim.origin.y .. claim.end.y {
                {
                    if let Some(c) = seen.get_mut(&(x, y)) {
                        if *c == 1 {
                            overlapped += 1;
                            *c += 1;
                        }
                        continue;
                    }
                }

                seen.insert((x, y), 1);
            }
        }

        for (claim_comp_id, claim_comp) in claims.iter() {
            if claim_id == claim_comp_id {
                continue;
            }

            if ! (
                (claim.end.x < claim_comp.origin.x || claim_comp.end.x < claim.origin.x) ||
                (claim.end.y < claim_comp.origin.y || claim_comp.end.y < claim.origin.y)
            ) {
                overlapping_ids.insert(*claim_id);
                overlapping_ids.insert(*claim_comp_id);
                break;
            }
        }
    }

    println!("Overlapped inches: {}", overlapped);
    println!("Isolated claim id: {:?}", ids.difference(&overlapping_ids));

    Ok(())
}
